(defsystem "spacebean"
  :version "0.1.0"
  :author "Momozor"
  :license "AGPL-3.0"
  :depends-on ("parenscript" "cl-who")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "Run and collect the keys!"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.md"))
  :in-order-to ((test-op (test-op "spacebean/tests"))))

(defsystem "spacebean/tests"
  :author "Momozor"
  :license "AGPL-3.0"
  :depends-on ("spacebean"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for spacebean"

  :perform (test-op (op c) (symbol-call :rove :run c)))
