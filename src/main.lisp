;;    Blue Beanie - A HTML5 Common Lisp + Parenscript web video game.
;;    Copyright (C) 2019 Momozor

;;    This program is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU Affero General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    This program is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU Affero General Public License for more details.

;;    You should have received a copy of the GNU Affero General Public License
;;    along with this program.  If not, see <https://www.gnu.org/licenses/>.
(defpackage spacebean
  (:use :cl :parenscript :cl-who)
  (:export :combine-all-into-single-js-file
           :sstring-binder
           :start-game-state
           :boot-class
           :load-class
           :menu-class
           :game-over-win-class
           :game-utils-class
           :game-restart-all-classes
           :game-module))
(in-package :spacebean)

(defparameter *application-root-path*
  (asdf:system-source-directory :spacebean))
(defun combine-all-into-single-js-file ()
  (with-open-file (f (merge-pathnames *application-root-path* "full-spacebean.js")
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    
    (write-sequence (boot-class) f)
    (write-sequence (load-class) f)
    (write-sequence (menu-class) f)
    (write-sequence (game-over-win-class) f)
    (write-sequence (game-utils-class) f)
    (write-sequence (game-restart-all-classes) f)
    (write-sequence (game-module) f)))

(defun sstring-binder (string1 string2)
  (format nil "~a~a" string1 string2))

;; access these via (ps (lisp object))
(defparameter *root-path* "assets")
(defparameter *rgraphics* (sstring-binder *root-path* "/graphics"))
(defparameter *raudio* (sstring-binder *root-path* "/audio"))
(defparameter *tilemaps* (sstring-binder *rgraphics* "/tilemaps"))
(defparameter *tiles* (sstring-binder *rgraphics* "/tiles"))
(defparameter *sprites* (sstring-binder *rgraphics* "/sprites"))
(defparameter *common-rgba* "rgba(0, 0, 0, 0.5)")

;; make loading assets less verbose and tedious
(defpsmacro assets-asm (method-type object-alias filepath &body body)
  `(chain game load (,method-type ,object-alias ,filepath ,@body)))

(defpsmacro common-screen-greet-style ()
  '(create
    "font" "bold 32px Arial"
    "fill" "#fff"
    "boundsAlignH" "center"
    "boundsAlignV" "middle"))

(defpsmacro defps-class (class-name &body body)
  `(defparameter ,class-name
     (lambda ,@body)))

(defpsmacro create-method-simple (class-name method-name &body body)
  `(setf (chain ,class-name prototype ,method-name)
         (lambda ,@body)))

(defpsmacro start-game-state (state-name)
  `(chain game state (start ,state-name)))

(defun boot-class ()
  (ps
    (defps-class *boot ()
      nil)

    (create-method-simple *boot create ()
                          (chain game physics
                                 (start-system (chain *Phaser *Physics *arcade*)))
                          (start-game-state "load")
                          nil)))



(defun load-class ()
  (ps
    (defps-class *load ()
      nil)

    (create-method-simple *load preload ()
                          (let* ((style (common-screen-greet-style))
                                 (text1 (chain game add
                                              (text 0 0 "Loading.." style))))
                            (chain text1 (set-shadow 3 3 (lisp *common-rgba*) 2))
                            (chain text1 (set-text-bounds 0 100 800 100))

                            (assets-asm tilemap "map_1" (+ (lisp *tilemaps*) "/tiled_1.json") nil
                                        (chain *phaser *tilemap *tiled_json*))
                            (assets-asm tilemap "map_2" (+ (lisp *tilemaps*) "/tiled_2.json") nil
                                        (chain *phaser *tilemap *tiled_json*))
                            (assets-asm tilemap "map_3" (+ (lisp *tilemaps*) "/tiled_3.json") nil
                                        (chain *phaser *tilemap *tiled_json*))
                            (assets-asm tilemap "map_4" (+ (lisp *tilemaps*) "/tiled_4.json") nil
                                        (chain *phaser *tilemap *tiled_json*))
                            (assets-asm tilemap "map_5" (+ (lisp *tilemaps*) "/tiled_5.json") nil
                                        (chain *phaser *tilemap *tiled_json*))

                            (assets-asm spritesheet "player" (+ (lisp *sprites*) "/MainGuySpriteSheet.png") 41 36 12)

                            (assets-asm image "landscape" (+ (lisp *tiles*) "/landscape.png"))
                            (assets-asm image "lava" (+ (lisp *tiles*) "/lava.png"))
                            (assets-asm image "door" (+ (lisp *tiles*) "/door.png"))
                            (assets-asm image "key" (+ (lisp *tiles*) "/key.png"))
                            (assets-asm image "wm" (+ (lisp *tiles*) "/wm.png"))
                            
                            (assets-asm spritesheet "bat" (+ (lisp *tiles*) "/bat-sprite32x32.png") 32 32 16)

                            (assets-asm audio "theme_music1" (+ (lisp *raudio*) "/theme_music1.mp3"))
                            (assets-asm audio "pick" (+ (lisp *raudio*) "/pick.wav"))
                            
                            (assets-asm audio "gmover_sfx" (+ (lisp *raudio*) "/game_over_sfx.wav"))
                            (assets-asm audio "calm_synth" (+ (lisp *raudio*) "/morningblue.mp3"))
                            nil))

    (create-method-simple *load create ()
                          (start-game-state "menu")
                          nil)))

(defun menu-class ()
  (ps
    (defps-class *menu ()
      nil)

    (create-method-simple *menu create ()
                          (let* ((style (common-screen-greet-style))
                                 (text1 (chain game add (text 0 0 "Blue Beanie" style)))
                                 (text2 (chain game add (text 0 40 "by Momozor" style)))
                                 (text3 (chain game add (text 0 150 "Click anywhere to play!" style))))
                            (chain text1 (set-shadow 3 3 (lisp *common-rgba*) 2))
                            (chain text1 (set-text-bounds 0 100 800 100))
                            (chain text2 (set-shadow 3 3 (lisp *common-rgba*) 2))
                            (chain text2 (set-text-bounds 0 100 800 100))
                            (chain text3 (set-shadow 3 3 (lisp *common-rgba*) 2))
                            (chain text3 (set-text-bounds 0 100 800 100))

                            (setf (chain this synth) (chain game add (audio "calm_synth")))
                            (chain this synth (loop-full 0.6))

                            nil))

    (create-method-simple *menu update ()
                          (when (chain game input active-pointer is-down)
                            (chain this synth (stop))
                            (start-game-state "level_one"))
                          nil)))

(defun game-over-win-class ()
  (ps
    (defps-class *game-over-win ()
      nil)

    (create-method-simple *game-over-win create ()
                          (let* ((style (common-screen-greet-style))
                                 (text1 (chain game add (text 0 0 "Congratulations! You WON!" style)))
                                 (text2 (chain game add (text 0 50 "Click anywhere on the screen to continue" style)))
                                 (win-music (chain game add (audio "game_win"))))

                            (chain text1 (set-shadow 3 3 (lisp *common-rgba*) 2))
                            (chain text1 (set-text-bounds 0 100 800 100))

                            (chain text2 (set-shadow 3 3 (lisp *common-rgba*) 2))
                            (chain text2 (set-text-bounds 0 100 800 100))

                            (chain win-music (play))
                            
                            nil))

    (create-method-simple *game-over-win update ()
                          (when (chain game input active-pointer is-down)
                            (start-game-state "menu"))
                          nil)))

(defun game-utils-class ()
  (ps
    
    (defps-class game-utils (player)
      (setf (chain this keyboard) (chain game input keyboard))
      (setf (chain this cursors) (chain this keyboard (create-cursor-keys)))
      (setf (chain this player) player)
      (setf (chain this theme_music) (chain game add (audio "theme_music1")))
      (setf (chain this collect_sfx) (chain game add (audio "pick")))
      (setf (chain this gmover_sfx) (chain game add (audio "gmover_sfx")))

      
      (chain this theme_music (loop-full 0.6))
      (chain this player animations (add "left" (list 9 10 11) 10 t))
      (chain this player animations (add "right" (list 3 4 5) 10 t))
      (chain this player animations (add "up" (list 6 7 8) 10 t))
      (chain this player animations (add "down" (list 0 1 2) 10 t))
      (chain this player animations (add "still" (list 1 ) nil t))

      (chain game physics arcade (enable (chain this player)))

      (chain game camera (follow (chain this player) (chain *phaser *camera *follow_lockon*) 0.1 0.1 ))
      
      
      nil)

    (create-method-simple game-utils set_main_player_movements ()
                          (cond ((or (chain this cursors left is-down)
                                     (chain this keyboard (is-down (chain *phaser *keyboard *a*))))
                                 (chain this player animations (play "left"))
                                 (setf (chain this player body velocity x) -175))

                                ((or (chain this cursors right is-down)
                                     (chain this keyboard (is-down (chain *phaser *keyboard *d*))))
                                 (chain this player animations (play "right"))
                                 (setf (chain this player body velocity x) 175))

                                ((or (chain this cursors up is-down)
                                     (chain this keyboard (is-down (chain *phaser *keyboard *w*))))
                                 (chain this player animations (play "up"))
                                 (setf (chain this player body velocity y) -175))

                                ((or (chain this cursors down is-down)
                                     (chain this keyboard (is-down (chain *phaser *keyboard *s*))))
                                 (chain this player animations (play "down"))
                                 (setf (chain this player body velocity y) 175))

                                (t
                                 (setf (chain this player body velocity x) 0)
                                 (setf (chain this player body velocity y) 0)
                                 (chain this player animations (play "still")))))

    (create-method-simple game-utils kill_theme_music ()
                          (chain this theme_music (stop)))

    (create-method-simple game-utils collect_key_sfx ()
                          (chain this collect_sfx (play)))

    (create-method-simple game-utils play_gmover_sfx ()
                          (chain this gmover_sfx (play)))))


;;; 'game-engine' logic


;; level restart
(defpsmacro game-restart-body (class-name level)
  `(progn (defps-class ,class-name () )
          
          (create-method-simple ,class-name create ()
                                (let* ((style (common-screen-greet-style))
                                       (text1 (chain game add (text 0 0 "Game Over! You lost!" style )))
                                       (text2 (chain game add (text 0 50 "Click anywhere on the screen to restart" style))))
                                  
                                  (chain text1 (set-shadow 3 3 (lisp *common-rgba*) 2))
                                  (chain text1 (set-text-bounds 0 100 800 100))
                                  (chain text2 (set-shadow 3 3 (lisp *common-rgba*) 2))
                                  (chain text2 (set-text-bounds 0 100 800 100)))
                                nil)

          (create-method-simple ,class-name update ()
                                (when (chain game input active-pointer is-down)
                                  (chain this (restart)))
                                nil)
          (create-method-simple ,class-name restart ()
                                (start-game-state ,level)
                                nil)))

(defun game-restart-all-classes ()
  (ps
    (game-restart-body game-restart-one "level_one")
    (game-restart-body game-restart-two "level_two")
    (game-restart-body game-restart-three "level_three")
    (game-restart-body game-restart-four "level_four")
    (game-restart-body game-restart-five "level_five")))

;; boot point

(defun game-module ()
  (ps
    (defparameter game (new (chain *phaser (*game 800 600 (chain *phaser *canvas*) "gameCanvas"))))

    (chain game state (add "boot" *boot))
    (chain game state (add "load" *load))
    (chain game state (add "menu" *menu))

    (chain game state (add "level_one" level-one))
    (chain game state (add "level_two" level-two))
    (chain game state (add "level_three" level-three))
    (chain game state (add "level_four" level-four))
    (chain game state (add "level_five" level-five))

    (chain game state (add "game_restart_one" game-restart-one))
    (chain game state (add "game_restart_two" game-restart-two))
    (chain game state (add "game_restart_three" game-restart-three))
    (chain game state (add "game_restart_four" game-restart-four))
    (chain game state (add "game_restart_five" game-restart-five))

    (chain game state (add "win" *game-over-win))

    (chain game state (start "boot"))))
