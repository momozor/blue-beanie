(defpackage spacebean/tests/main
  (:use :cl
        :spacebean
        :parenscript
        :rove))
(in-package :spacebean/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :spacebean)' in your Lisp.

(deftest spacebean
  (testing "should join strings together"
    (ok (string-equal (sstring-binder "qu" "ack")
                      "quack")))
  (testing "boot-class"
    (ok (string-equal (boot-class)
                      "var Boot = function () {
    return null;
};
Boot.prototype.create = function () {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.state.start('load');
    return null;
};")))
  (testing "load-class"
    (ok (string-equal (load-class)
                      "var Load = function () {
    return null;
};
Load.prototype.preload = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Loading..', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    game.load.tilemap('map_1', 'assets/graphics/tilemaps' + '/tiled_1.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map_2', 'assets/graphics/tilemaps' + '/tiled_2.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map_3', 'assets/graphics/tilemaps' + '/tiled_3.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map_4', 'assets/graphics/tilemaps' + '/tiled_4.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map_5', 'assets/graphics/tilemaps' + '/tiled_5.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.spritesheet('player', 'assets/graphics/sprites' + '/MainGuySpriteSheet.png', 41, 36, 12);
    game.load.spritesheet('gamepad', 'assets/graphics/sprites' + '/gamepad_spritesheet.png', 100, 100);
    game.load.image('landscape', 'assets/graphics/tiles' + '/landscape.png');
    game.load.image('lava', 'assets/graphics/tiles' + '/lava.png');
    game.load.image('door', 'assets/graphics/tiles' + '/door.png');
    game.load.image('key', 'assets/graphics/tiles' + '/key.png');
    game.load.image('wm', 'assets/graphics/tiles' + '/wm.png');
    game.load.spritesheet('bat', 'assets/graphics/tiles' + '/bat-sprite32x32.png', 32, 32, 16);
    game.load.audio('theme_music1', 'assets/audio' + '/theme_music1.mp3');
    game.load.audio('pick', 'assets/audio' + '/pick.wav');
    game.load.audio('gmover_sfx', 'assets/audio' + '/game_over_sfx.wav');
    game.load.audio('calm_synth', 'assets/audio' + '/morningblue.mp3');
    return null;
};
Load.prototype.create = function () {
    game.state.start('menu');
    return null;
};")))
  (testing "menu-class"
    (ok (string-equal (menu-class)
                      "var Menu = function () {
    return null;
};
Menu.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Blue Beanie', style);
    var text2 = game.add.text(0, 40, 'by Momozor', style);
    var text3 = game.add.text(0, 150, 'Click anywhere to play!', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    text3.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text3.setTextBounds(0, 100, 800, 100);
    this.synth = game.add.audio('calm_synth');
    this.synth.loopFull(0.6);
    return null;
};
Menu.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        this.synth.stop();
        game.state.start('level_one');
    };
    return null;
};")))
  (testing "game-over-win-class"
    (ok (string-equal (game-over-win-class)
                      "var GameOverWin = function () {
    return null;
};
GameOverWin.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Congratulations! You WON!', style);
    var text2 = game.add.text(0, 50, 'Click anywhere on the screen to continue', style);
    var winMusic = game.add.audio('game_win');
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    winMusic.play();
    return null;
};
GameOverWin.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        game.state.start('menu');
    };
    return null;
};")))
  (testing "game-utils-class"
    (ok (string-equal (game-utils-class)
                      "var gameUtils = function (player) {
    this.keyboard = game.input.keyboard;
    this.cursors = this.keyboard.createCursorKeys();
    this.player = player;
    this.theme_music = game.add.audio('theme_music1');
    this.collect_sfx = game.add.audio('pick');
    this.gmover_sfx = game.add.audio('gmover_sfx');
    this.theme_music.loopFull(0.6);
    this.player.animations.add('left', [9, 10, 11], 10, true);
    this.player.animations.add('right', [3, 4, 5], 10, true);
    this.player.animations.add('up', [6, 7, 8], 10, true);
    this.player.animations.add('down', [0, 1, 2], 10, true);
    this.player.animations.add('still', [1], null, true);
    game.physics.arcade.enable(this.player);
    game.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    return null;
};
gameUtils.prototype.set_main_player_movements = function () {
    if (this.cursors.left.isDown || this.keyboard.isDown(Phaser.Keyboard.A)) {
        this.player.animations.play('left');
        return this.player.body.velocity.x = -175;
    } else if (this.cursors.right.isDown || this.keyboard.isDown(Phaser.Keyboard.D)) {
        this.player.animations.play('right');
        return this.player.body.velocity.x = 175;
    } else if (this.cursors.up.isDown || this.keyboard.isDown(Phaser.Keyboard.W)) {
        this.player.animations.play('up');
        return this.player.body.velocity.y = -175;
    } else if (this.cursors.down.isDown || this.keyboard.isDown(Phaser.Keyboard.S)) {
        this.player.animations.play('down');
        return this.player.body.velocity.y = 175;
    } else {
        this.player.body.velocity.x = 0;
        this.player.body.velocity.y = 0;
        return this.player.animations.play('still');
    };
};
gameUtils.prototype.kill_theme_music = function () {
    return this.theme_music.stop();
};
gameUtils.prototype.collect_key_sfx = function () {
    return this.collect_sfx.play();
};
gameUtils.prototype.play_gmover_sfx = function () {
    return this.gmover_sfx.play();
};")))
  (testing "game-module"
    (ok (string-equal (game-module)
                      "var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'gameCanvas');
game.state.add('boot', Boot);
game.state.add('load', Load);
game.state.add('menu', Menu);
game.state.add('level_one', levelOne);
game.state.add('level_two', levelTwo);
game.state.add('level_three', levelThree);
game.state.add('level_four', levelFour);
game.state.add('level_five', levelFive);
game.state.add('game_restart_one', gameRestartOne);
game.state.add('game_restart_two', gameRestartTwo);
game.state.add('game_restart_three', gameRestartThree);
game.state.add('game_restart_four', gameRestartFour);
game.state.add('game_restart_five', gameRestartFive);
game.state.add('win', GameOverWin);
game.state.start('boot');"
                      )))
  (testing "game-restart-all-classes"
    (ok (string-equal (game-restart-all-classes)
                      "var gameRestartOne = function () {
    return null;
};
gameRestartOne.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Game Over! You lost!', style);
    var text2 = game.add.text(0, 50, 'Click anywhere on the screen to restart', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    return null;
};
gameRestartOne.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        this.restart();
    };
    return null;
};
gameRestartOne.prototype.restart = function () {
    game.state.start('level_one');
    return null;
};
var gameRestartTwo = function () {
    return null;
};
gameRestartTwo.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Game Over! You lost!', style);
    var text2 = game.add.text(0, 50, 'Click anywhere on the screen to restart', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    return null;
};
gameRestartTwo.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        this.restart();
    };
    return null;
};
gameRestartTwo.prototype.restart = function () {
    game.state.start('level_two');
    return null;
};
var gameRestartThree = function () {
    return null;
};
gameRestartThree.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Game Over! You lost!', style);
    var text2 = game.add.text(0, 50, 'Click anywhere on the screen to restart', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    return null;
};
gameRestartThree.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        this.restart();
    };
    return null;
};
gameRestartThree.prototype.restart = function () {
    game.state.start('level_three');
    return null;
};
var gameRestartFour = function () {
    return null;
};
gameRestartFour.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Game Over! You lost!', style);
    var text2 = game.add.text(0, 50, 'Click anywhere on the screen to restart', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    return null;
};
gameRestartFour.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        this.restart();
    };
    return null;
};
gameRestartFour.prototype.restart = function () {
    game.state.start('level_four');
    return null;
};
var gameRestartFive = function () {
    return null;
};
gameRestartFive.prototype.create = function () {
    var style = { 'font' : 'bold 32px Arial',
                  'fill' : '#fff',
                  'boundsAlignH' : 'center',
                  'boundsAlignV' : 'middle'
                };
    var text1 = game.add.text(0, 0, 'Game Over! You lost!', style);
    var text2 = game.add.text(0, 50, 'Click anywhere on the screen to restart', style);
    text1.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text1.setTextBounds(0, 100, 800, 100);
    text2.setShadow(3, 3, 'rgba(0, 0, 0, 0.5)', 2);
    text2.setTextBounds(0, 100, 800, 100);
    return null;
};
gameRestartFive.prototype.update = function () {
    if (game.input.activePointer.isDown) {
        this.restart();
    };
    return null;
};
gameRestartFive.prototype.restart = function () {
    game.state.start('level_five');
    return null;
};"))))
